(function () {
	window.addEventListener("tizenhwkey", function (ev) {
		var activePopup = null,
			page = null,
			pageid = "";

		if (ev.keyName === "back") {
			activePopup = document.querySelector(".ui-popup-active");
			page = document.getElementsByClassName("ui-page-active")[0];
			pageid = page ? page.id : "";

			if (pageid === "main" && !activePopup) {
				try {
					tizen.application.getCurrentApplication().exit();
				} catch (ignore) {
				}
			} else {
				window.history.back();
			}
		}
	});
}());

window.onload=function() {
	var state;
	try {
		state = JSON.parse(window.localStorage.getItem('state') || "{}");
	} catch(e) {
		state = {};
	}
	
	
    var button1 = document.getElementById("button1");
//    button1.addEventListener("click", function() {
//    	var alarm1 = new tizen.AlarmRelative(1*tizen.alarm.PERIOD_MINUTE);
//    	tizen.alarm.add(alarm1, tizen.application.getAppInfo().id);
//    });
    button1.addEventListener("click", function() {
    	console.log("Button 1 clicked.");
    });
    var rotaryPosition = 0;
    document.addEventListener("rotarydetent", function(ev) {
    	var direction = ev.detail.direction;
    	if(direction === "CW") {
    		rotaryPosition++;
    	}
    	if(direction === "CCW") {
    		rotaryPosition--;
    	}
    	console.log("Rotary position: "+rotaryPosition);
    });
}